const WIDTH = 800;
const HEIGHT = 500;
const canvas = document.getElementById('canvas');
canvas.width = WIDTH;
canvas.height = HEIGHT;

const ctx = canvas.getContext('2d');
const array = [];
const N = 10;

const rectangle1 = {x1: 0, y1: 0, x2: 0, y2: 0};
const rectangle2 = {x1: 0, y1: 0, x2: 0, y2: 0};

function generatorCoordinate(obj) {
    for (let key in obj) {
        if(key + '' === 'x1' || key + '' === 'x2') {
            obj[key] = randomInteger(0, WIDTH);
        }else {
            obj[key] = randomInteger(0, HEIGHT);
        }
    }
}

function randomInteger(min, max) {
    let rand = min + Math.random() * (max + 1 - min);
    return Math.floor(rand);
}

generatorCoordinate(rectangle1);
generatorCoordinate(rectangle2);

function upgradeRectangleCoordinates(rec) {
    let rx1, rx2, ry1, ry2;

    if (rec.x1 > rec.x2) {
        rx1 = rec.x2; rx2 = rec.x1;
    } else {
        rx1 = rec.x1; rx2 = rec.x2;
    }

    if (rec.y1 > rec.y2) {
        ry1 = rec.y2; ry2 = rec.y1;
    } else {
        ry1 = rec.y1; ry2 = rec.y2;
    }

    return {x1: rx1, y1: ry1, x2: rx2, y2: ry2};
}

let upgradedRectangle1 = upgradeRectangleCoordinates(rectangle1);
let upgradedRectangle2 = upgradeRectangleCoordinates(rectangle2);

ctx.strokeRect(upgradedRectangle1.x1, upgradedRectangle1.y1, upgradedRectangle1.x2 - upgradedRectangle1.x1, upgradedRectangle1.y2 - upgradedRectangle1.y1);
ctx.strokeRect(upgradedRectangle2.x1, upgradedRectangle2.y1, upgradedRectangle2.x2 - upgradedRectangle2.x1, upgradedRectangle2.y2 - upgradedRectangle2.y1);

function intersectionPointsRectangle(a, b) {

    let left = Math.max(a.x1, b.x1);
    let top = Math.max(a.y1, b.y1);
    let right = Math.min(a.x2, b.x2);
    let bottom = Math.min(a.y2, b.y2);

    let width = right - left;
    let height = bottom - top;

    if ((width <= 0) || (height <= 0))
        return 0;

    // If you want to consider the touch of rectangles as intersecting.
    // if ((width < 0) || (height < 0))
    //     return 0;

    return {x1: left, y1: top, x2: right, y2: bottom};


}

let rectangle3 = intersectionPointsRectangle(upgradedRectangle1, upgradedRectangle2);

if(rectangle3 !== 0) {
    let centerX_rec3 = ((rectangle3.x2 - rectangle3.x1) / 2) + rectangle3.x1;
    let centerY_rec3 = ((rectangle3.y2 - rectangle3.y1) / 2) + rectangle3.y1;
    let radiusX_rec3 = (rectangle3.x2 - rectangle3.x1) / 2;
    let radiusY_rec3 = (rectangle3.y2 - rectangle3.y1) / 2;

    ctx.strokeStyle = "red";
    ctx.strokeRect(rectangle3.x1, rectangle3.y1, rectangle3.x2 - rectangle3.x1, rectangle3.y2 - rectangle3.y1);

    ctx.beginPath();
    ctx.strokeStyle = "green";
    ctx.ellipse(centerX_rec3, centerY_rec3, radiusX_rec3, radiusY_rec3, Math.PI, 0, 2 * Math.PI);
    ctx.stroke();

    generatePoints(centerX_rec3, centerY_rec3, radiusX_rec3, radiusY_rec3, N);
}

function drawPoint(x, y) {
    ctx.beginPath();
    ctx.strokeStyle = "blue";
    ctx.arc(x, y, 1, 0, 2 * Math.PI);
    ctx.stroke();
}

function generatePoints(centerX, centerY, radiusX, radiusY, N) {
    let t, d , x, y;
    for(let i = 0; i < N; i++) {
        t = 2 * Math.PI * Math.random();
        d = Math.sqrt(Math.random());
        x = centerX + radiusX * d * Math.cos(t);
        y = centerY + radiusY * d * Math.sin(t);
        drawPoint(x, y);
        array.push({x: x, y: y});
    }
}